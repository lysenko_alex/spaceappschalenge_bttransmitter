#ifndef BLUETOOTHSERVER_H
#define BLUETOOTHSERVER_H

#include <QObject>
#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServer>
#include <QList>
#include <QLowEnergyController>
#include <QDebug>
#include "controller.h"

class BluetoothServer : public QObject
{
    Q_OBJECT

public:
    BluetoothServer(QObject *parent = nullptr);
    ~BluetoothServer();
    
    //agent discovery
    void startDeviceDescovery();

    //clear object
    void stopServer();

public slots:
    //agent discovery
    void printDevice(const QBluetoothDeviceInfo & device);
    void serviceDiscoverFinished();
    
    //server
    void clientConnected();
    void errorHandler(QBluetoothServer::Error error);
    
    //local device
    void onLocalDeviceConnect(const QBluetoothAddress & dev);
    void onLocalDeviceDisconnect(const QBluetoothAddress &address);
    
    //socket
    void readSocket();
    void onSocketDisconnect();
    void onStateChanged(QBluetoothSocket::SocketState state);
    void onSocketConnect();
    void onSocketError(QBluetoothSocket::SocketError error);

private:
    QBluetoothDeviceDiscoveryAgent * discoveryAgent;
    QBluetoothLocalDevice * localDevice;
    QBluetoothServer * server;
    QBluetoothServiceInfo * serverInfo;

    QList<QBluetoothSocket *> clientSockets;
};

#endif // BLUETOOTHSERVER_H
