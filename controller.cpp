#include "controller.h"

Controller::Controller(QObject *parent) : QObject(parent)
{

}

Controller &Controller::instance()
{
    static Controller s;
    return s;
}

void Controller::gpioInit()
{
    gpioProcess.start("gpio", {"mode", "1", "pwm"});
    gpioProcess.waitForFinished();
    gpioProcess.start("gpio", {"mode", "24", "pwm"});
    gpioProcess.waitForFinished();

    changeState(0, 0);
}


void Controller::changeState(int fb, int rl)
{
    QString errorStr;
    int rl_pwm;
    int fb_pwm;

    if (rl >= 0) {
        rl_pwm = rl_zero + (abs(rl) * rl_zero / 10);
    }
    else
        rl_pwm = rl_zero - (abs(rl) * rl_zero / 10);


    if (fb < 0)
        fb_pwm = 465;
    else if (fb > 0)
        fb_pwm = 400;
    else
        fb_pwm = fb_zero;



    qDebug() << "FB:" << fb << "-" << fb_pwm;
    qDebug() << "RL:" << rl << "-" << rl_pwm;


    gpioProcess.start("gpio", {"pwm", "1", QString::number(fb_pwm)});
    gpioProcess.waitForFinished();
    errorStr = gpioProcess.readAllStandardError();

    if (!errorStr.isEmpty())
        qDebug() << "Error:" << errorStr;

    gpioProcess.start("gpio", {"pwm", "24", QString::number(rl_pwm)});
    gpioProcess.waitForFinished();
    errorStr = gpioProcess.readAllStandardError();

    if (!errorStr.isEmpty())
        qDebug() << "Error:" << errorStr;
}
