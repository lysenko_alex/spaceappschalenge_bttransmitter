#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QProcess>
#include <QDebug>

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);

    static Controller& instance();

private:
    QProcess gpioProcess;
    int rl_zero = 440;
    int fb_zero = 440;

public slots:
    void gpioInit();
    void changeState(int fb, int rl);
};

#endif // CONTROLLER_H
