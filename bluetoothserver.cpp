#include "bluetoothserver.h"

#include <iostream>

BluetoothServer::BluetoothServer(QObject *parent)
    : QObject(parent)
{
    localDevice = new QBluetoothLocalDevice(this);
    if (localDevice->isValid())
    {
        qDebug() << "Bluetooth is available on this device";
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        localDevice->powerOn();
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        qDebug() << "Local device: " << localDevice->name() << " ("
                 << localDevice->address().toString().trimmed() << ")";

        connect(localDevice, SIGNAL(deviceConnected(const QBluetoothAddress &)),
                this, SLOT(onLocalDeviceConnect(const QBluetoothAddress &)));
        connect(localDevice, SIGNAL(deviceDisconnected(const QBluetoothAddress &)),
                this, SLOT(onLocalDeviceDisconnect(const QBluetoothAddress &)));

        server = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, this);
        connect(server, SIGNAL(newConnection()),
            this, SLOT(clientConnected()));
        connect(server, SIGNAL(error(QBluetoothServer::Error)),
            this, SLOT(errorHandler(QBluetoothServer::Error)));

        qDebug() << "Max connection" << server->maxPendingConnections();
        qDebug() << "Security" << server->securityFlags();


        auto info = server->listen(QBluetoothUuid::SerialPort);
        qDebug() << info.serviceUuid();
        qDebug() << server->serverAddress() << server->serverPort();
    }
    else
        qDebug() << "Bluetooth is not available on this device";

    Controller::instance().gpioInit();
}

BluetoothServer::~BluetoothServer()
{
    stopServer();
}

//Server slots

void BluetoothServer::errorHandler(QBluetoothServer::Error error)
{
    qDebug() << error;
}

void BluetoothServer::clientConnected()
{
    QBluetoothSocket *socket = server->nextPendingConnection();
    if (!socket)
    {
        qWarning() << "Can't retrieve socket";
        return;
    }

    qDebug() << socket->localAddress();
    qDebug() << socket->localName();
    qDebug() << socket->localPort();
    qDebug() << socket->peerAddress();
    qDebug() << socket->peerName();
    qDebug() << socket->peerPort();
    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(onSocketDisconnect()));
    connect(socket, SIGNAL(connected()), this, SLOT(onSocketConnect()));
    connect(socket, SIGNAL(error(QBluetoothSocket::SocketError)),
            this, SLOT(onSocketError(QBluetoothSocket::SocketError)));
    connect(socket, SIGNAL(stateChanged(QBluetoothSocket::SocketState)),
            this, SLOT(onStateChanged(QBluetoothSocket::SocketState)));
    clientSockets.append(socket);
    qDebug() << socket->peerName() << "connected!";
}

//Socket slots

void BluetoothServer::readSocket()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    while (socket->canReadLine())
    {
        QByteArray line = socket->readLine().trimmed();
        int num0, num1;
        num0 = static_cast<qint8>(line[0]);
        num1 = static_cast<qint8>(line[1]);

        if (std::abs(num0) < 6 && std::abs(num1) < 6)
        {
            qDebug() << num0 << num1;
            Controller::instance().changeState(num0, num1);
        }
    }
}

void BluetoothServer::onSocketDisconnect()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    qDebug() << "Socket disconnected";

    clientSockets.removeOne(socket);
    socket->deleteLater();
}


void BluetoothServer::onSocketConnect()
{
    qDebug() << "Socket connected";
}

void BluetoothServer::onSocketError(QBluetoothSocket::SocketError error)
{
    qDebug() << "ERROR";
    qDebug() << error;
}

void BluetoothServer::onStateChanged(QBluetoothSocket::SocketState state)
{
    qDebug() << state;
}

//Discovery agent (unused)

void BluetoothServer::startDeviceDescovery()
{
    // Create a discovery agent and connect to its signals
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent();
    discoveryAgent->error();
    qDebug() << QBluetoothDeviceDiscoveryAgent::supportedDiscoveryMethods();
    /*bool found = discoveryAgent->setRemoteAddress(QBluetoothAddress("7C:D6:61:50:A5:F3"));
    if (!found)
        qDebug() << "Could not locate device";*/
    connect(discoveryAgent, SIGNAL(deviceDiscovered(const QBluetoothDeviceInfo &)),
            this, SLOT(printDevice(const QBluetoothDeviceInfo &)));
    connect(discoveryAgent, SIGNAL(finished()),
            this, SLOT(serviceDiscoverFinished()));

    // Start a discovery
    discoveryAgent->start();
}

//Discovery agent`s slots (unused)

void BluetoothServer::serviceDiscoverFinished()
{
    std::cout << "Discover finished" << std::endl;
}

void BluetoothServer::printDevice(const QBluetoothDeviceInfo & device)
{
    qDebug() << "Found new device:" << device.name() << device.coreConfigurations()
             << device.serviceClasses();
}

//Local device slots

void BluetoothServer::onLocalDeviceConnect(const QBluetoothAddress & dev)
{
    qDebug() << "Local connected";
    qDebug() << localDevice->pairingStatus(dev);
    qDebug() << dev;
}

void BluetoothServer::onLocalDeviceDisconnect(const QBluetoothAddress & address)
{
    qDebug() << "Local disconnected";
    qDebug() << address;
}

void BluetoothServer::stopServer()
{
    // Unregister service
    serverInfo->unregisterService();
    delete serverInfo;
    server = nullptr;

    // Close sockets
    qDeleteAll(clientSockets);

    // Close server
    delete server;
    server = nullptr;
    
    delete localDevice;
    delete discoveryAgent;
}
