TEMPLATE = app
CONFIG += console c++11
QT += core
QT += bluetooth
QMAKE_CXXFLAGS += "-Wall -Wextra -Wpedantic "

SOURCES += \
    controller.cpp \
        main.cpp \
	bluetoothserver.cpp

HEADERS += \
	bluetoothserver.h \
	controller.h

target.path = /home/pi/Projects/qt_bluetooth
INSTALLS += target

